(ns private-methods.politician)

(defn speak []
  "I sacrifice myself for you!")

(defn- truth []
  "I loot you. HA HA HA!!!!")

(defn ^:private super-secret []
  "I have your money in Swiss bank. I will escape to Europe.")

(defn torture-him []
  (truth))

(defn revolt []
  (super-secret))

(def meaning-of-life 42)

 ;; (def- meaning-of-death "to trust a politician") ;; throws error
