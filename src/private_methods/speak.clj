(ns private-methods.speak
  (:require [private-methods.politician :as p]))

(p/speak)
(p/truth)
(p/torture-him)
(p/revolt)

(p/meaning-of-life) ;; error

(+ p/meaning-of-life 1)

(def meaning-of-life p/meaning-of-life)

(println meaning-of-life)
